using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollectChest : MonoBehaviour
{
    public int iLevelToLoad;
    public string sLevelToLoad;
    public bool useIntegerToLoadLevel = false;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    void onTrigger(Collider collision)
    {
        GameObject collisionGameObject = collision.gameObject;

        if(collision.gameObject.tag == "Player")
        {
            SceneManager.LoadScene("Summer");
        }
    }
}
