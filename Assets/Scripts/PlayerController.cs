using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    // variaveis para controle 
    private CharacterController controller;
    private Animator anim;

    private float mouseX = 0.0f, mouseY = 0.0f;

    private int activeScene;

    public float speed;
    public float gravity;
    public float rotSpeed;
    public float sensibilidade = 2.0f;

    public HealthBar healthBar;
    public int maxHealth = 100;
    public int currenthealth;

    public StaminaBar staminabar;
    public float maxStamina = 100f;
    public float currentstamina;

    /*public CollectCrystalBar crystalBar;
    public float maxCollect = 100f;
    public float currentCollect;
    public GameObject CrystalOff;
    private GameObject CountCollect = null;
    public int CollectedCrystals = 0;
    public Text CollectedCrystalsText;*/

    private Vector3 moveDirection;

    private void Awake()
    {
        activeScene = SceneManager.GetActiveScene().buildIndex;
    }

    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponent<Animator>();

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        currenthealth = maxHealth;
        //healthBar.maxHealth(maxHealth);

        currentstamina = maxStamina;
        staminabar.maxStamina(maxStamina);

        //currentCollect = 0;
        //crystalBar.CompleteCollect(maxCollect);
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Attack();
    }

    void Move()
    {

        if (Input.GetKey(KeyCode.W))
        {
            if (currentstamina > 60 && Input.GetKey(KeyCode.LeftShift))
            {
                moveDirection = Vector3.forward * speed * 2;
                anim.SetInteger("transition", 2);
                currentstamina -= 0.2f;
            } else if (currentstamina > 25) {
                moveDirection = Vector3.forward * speed;
                anim.SetInteger("transition", 1);
                currentstamina -= 0.1f;
            } else
            {
                moveDirection = Vector3.forward * speed / 2;
                anim.SetInteger("transition", 4);
                currentstamina -= 0.05f;
            }
        } else
        {
        currentstamina += 0.4f;
        }
            
        if (Input.GetKeyUp(KeyCode.W))
        {
            moveDirection = Vector3.zero;
            anim.SetInteger("transition", 0);
        }
        staminabar.SetStamina(currentstamina);

        moveDirection.y -= gravity * Time.deltaTime;
        moveDirection = transform.TransformDirection(moveDirection);

        controller.Move(moveDirection * Time.deltaTime);

        mouseX += Input.GetAxis("Mouse X") * sensibilidade;
        mouseY -= Input.GetAxis("Mouse Y") * sensibilidade;
        transform.eulerAngles = new Vector3(0, mouseX, 0);
    }

    void Attack()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            anim.SetInteger("transition", 3);
        }
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            anim.SetInteger("transition", 0);
        }
    }

    private void OnTriggerEnter(Collider other)
    {     
        if (other.CompareTag("Chest"))
        {
            SceneManager.LoadScene(activeScene + 1);
        }
    }

    //healthBar.setHealth(currentHealth);
}
