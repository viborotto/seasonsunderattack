using UnityEngine;
using UnityEngine.UI;

public class StaminaBar : MonoBehaviour
{
    public Slider slider;

    public void maxStamina(float Stamina)
    {
        slider.maxValue = Stamina;
        slider.value = Stamina;
    }

    public void SetStamina(float Stamina)
    {
        slider.value = Stamina;
    }
}
