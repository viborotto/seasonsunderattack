using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CrystalCollect : MonoBehaviour
{
    public CollectCrystalBar crystalBar;
    private float maxCollect = 100f;
    private float currentCollect;
    public GameObject CrystalOff;
    public GameObject CountCollect;
    public Text CollectedCrystalsText;

    private void Start()
    {
        crystalBar.CompleteCollect(maxCollect);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            CountCollect.SetActive(true);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        int a;
        if (other.CompareTag("Player"))
        {
            if (Input.GetKey(KeyCode.E))
            {
                currentCollect += 0.5f;
                crystalBar.SetFill(currentCollect);
                if (currentCollect == 100)
                {
                    Instantiate(CrystalOff, transform.position, transform.rotation);
                    Destroy(transform.gameObject);
                    CountCollect.SetActive(false);
                    int.TryParse(CollectedCrystalsText.text.ToString(), out a);
                    a++;
                    CollectedCrystalsText.text = a.ToString();
                }
            }
            if (Input.GetKeyUp(KeyCode.E))
            {
                crystalBar.SetFill(0f);
                currentCollect = 0;
            }
        }  
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            CountCollect.SetActive(false);
            crystalBar.SetFill(0);
        }
    }
}
