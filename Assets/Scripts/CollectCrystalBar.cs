using UnityEngine;
using UnityEngine.UI;

public class CollectCrystalBar : MonoBehaviour
{
    public Slider slider;

    public void CompleteCollect(float Press)
    {
        slider.maxValue = Press;
        slider.value = 0;
    }

    public void SetFill(float Press)
    {
        slider.value = Press;
    }
}
